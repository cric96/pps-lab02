package u02lab.code;

public interface GeneratorFactory {
    SequenceGenerator random(int elements);

    SequenceGenerator range(int start, int stop);
}
