package u02lab.code;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

class GeneratorImpl implements SequenceGenerator {
    public static final int CURRENT_ELEMENT = 0;
    private List<Integer> elementsRemaining;
    private Supplier<List<Integer>> elementProducer;

    GeneratorImpl(final Supplier<List<Integer>> elementProducer) {
        this.elementProducer = elementProducer;
        reset();
    }

    @Override
    public Optional<Integer> next() {
        return isOver() ? Optional.empty() : Optional.of(elementsRemaining.remove(CURRENT_ELEMENT));
    }

    @Override
    public void reset() {
        elementsRemaining = elementProducer.get();
    }

    @Override
    public boolean isOver() {
        return elementsRemaining.isEmpty();
    }

    @Override
    public List<Integer> allRemaining() {
        return Collections.unmodifiableList(elementsRemaining);
    }
}
