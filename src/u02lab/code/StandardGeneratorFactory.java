package u02lab.code;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StandardGeneratorFactory implements GeneratorFactory {
    private static final GeneratorFactory SINGLETON = new StandardGeneratorFactory();
    private static final String RANDOM_EXCEPTION = "random generator must generate a positive number of elements";
    private static final String RANGE_EXCEPTION = "start must be greater the stop";

    public static final GeneratorFactory instance() {
        return SINGLETON;
    }
    @Override
    public SequenceGenerator random(int elements) {
        if(elements < 0){
            throw new IllegalArgumentException(RANDOM_EXCEPTION);
        }
        final Random random = new Random();
        return new GeneratorImpl(() -> IntStream.range(0,elements).map(x -> random.nextInt(1)).boxed().collect(Collectors.toList()));
    }
;
    @Override
    public SequenceGenerator range(int start, int stop) {
        if(start > stop) {
            throw new IllegalArgumentException(RANGE_EXCEPTION);
        }
        return new GeneratorImpl(() -> IntStream.rangeClosed(start,stop).boxed().collect(Collectors.toList()));
    }
}
