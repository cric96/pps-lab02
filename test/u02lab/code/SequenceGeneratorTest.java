package u02lab.code;

public class SequenceGeneratorTest {
    protected SequenceGenerator generator;

    protected void performNextMultipleTimes(final int times) {
        for(int i = 0; i < times; i ++) {
            generator.next();
        }
    }

    protected void consumeGenerator() {
        while (generator.next().isPresent()) {}
    }

    protected static class Range {
        private final int start;
        private final int stop;


        public Range(int start, int stop) {
            this.start = start;
            this.stop = stop;
        }

        public int getStart() {
            return start;
        }

        public int getStop() {
            return stop;
        }
    }
}
