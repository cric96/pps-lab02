package u02lab.code;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class NotEmptyRandomGeneratorTest extends SequenceGeneratorTest{
    private static final int ELEMENTS = 10;

    @Before
    public void init() {
        generator = StandardGeneratorFactory.instance().random(ELEMENTS);
    }

    @Test
    public void initiallyNotOver() {
        Assert.assertFalse(generator.isOver());
    }

    @Test
    public void initiallyNextNotEmpty() {
        Assert.assertTrue(generator.next().isPresent());
    }

    @Test
    public void remainingEqualsToElementsNumber() {
        Assert.assertEquals(generator.allRemaining().size(), ELEMENTS);
    }

    @Test
    public void whenIsOverNextIsEmpty() {
        consumeGenerator();
        Assert.assertEquals(generator.next(), Optional.empty());
    }

    @Test
    public void afterMultipleNextGeneratorHasNext() {
        performNextMultipleTimes(ELEMENTS / 2);
        Assert.assertTrue(generator.next().isPresent());
    }

    @Test
    public void nextIsEqualsToOneOrZero() {
        final Optional<Integer> current = generator.next();
        Assert.assertTrue(current.equals(Optional.of(1)) || current.equals(Optional.of(0)));
    }
}
