package u02lab.code;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RunWith(Parameterized.class)
public class NotEmptyRangeGeneratorTest extends SequenceGeneratorTest {

    private final Integer start;
    private final Integer stop;
    public  final int middle;
    private final List<Integer> allElements;

    @Parameterized.Parameters
    public static List<Range> data() {
        return Arrays.asList(new Range(0,10),
                new Range(-11,44),
                new Range(-100,444),
                new Range(100,400));
    }
    public NotEmptyRangeGeneratorTest(final Range range) {
        start = range.getStart();
        stop = range.getStop();
        this.generator = StandardGeneratorFactory.instance().range(start, stop);
        allElements = IntStream.rangeClosed(start,stop).boxed().collect(Collectors.toList());
        middle = (start + stop) / 2;
    }

    @Test
    public void initiallyNotEmpty() {
        Assert.assertTrue(generator.next().isPresent());
    }

    @Test
    public void initiallyNotOver() {
        Assert.assertFalse(generator.isOver());
    }

    @Test
    public void firstElementEqualsToStart() {
        Assert.assertEquals(generator.next(), Optional.of(start));
    }

    @Test
    public void lastElementEqualStop(){
        performNextMultipleTimes(stop - start);
        Assert.assertEquals(generator.next(), Optional.of(stop));
    }

    @Test
    public void ifIsOverNextIsEmpty() {
        consumeGenerator();
        Assert.assertTrue(generator.isOver() && !generator.next().isPresent());
    }

    @Test
    public void remainingSizeIsCorrect() {
        Assert.assertEquals(generator.allRemaining().size(), allElements.size());
    }


    @Test
    public void resetRestartGenerator() {
        performNextMultipleTimes(middle);
        generator.reset();
        Assert.assertEquals(generator.next(), Optional.of(start));
    }

    @Test
    public void isOverFalseAverMultipleNext() {
        performNextMultipleTimes(middle);
        Assert.assertFalse(generator.isOver());
    }

}
