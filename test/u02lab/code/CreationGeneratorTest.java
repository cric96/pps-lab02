package u02lab.code;

import org.junit.Assert;
import org.junit.Test;

import java.util.function.Supplier;

public class CreationGeneratorTest {
    private static final int START = 10;
    private static final int STOP = 0;
    private static final int ELEMENTS = -1;

    @Test
    public void startMustBeGreaterThenStop() {
        checkThrows(() -> StandardGeneratorFactory.instance().range(START,STOP));
    }

    @Test
    public void elementsMustBePositiveInRandom() {
        checkThrows(() -> StandardGeneratorFactory.instance().random(ELEMENTS));
    }

    private void checkThrows(Supplier<SequenceGenerator> creator) {
        try {
            creator.get();
            Assert.fail();
        } catch (IllegalArgumentException exc) {

        } catch (Exception exc) {
            Assert.fail();
        }
    }
}
